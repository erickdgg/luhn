function Validar(tarjeta) {
    var numero = 0;


    tarjeta = prompt('Ingresa un número de tarjeta');
    if (tarjeta === null || (isNaN(tarjeta) === true)) {
        alert('Ingrese un número de tarjeta válido');
        return Validar(tarjeta);
    }

    var sum = 0,
        alt = true,
        i = tarjeta.length - 1,
        num;

    if (tarjeta.length < 10 || tarjeta.length > 15) {
        alert('El número de tarjeta tiene que ser de 10 a 15 digitos.');
        return Validar(tarjeta);
    }

    while (i >= 0) {
        num = parseInt(tarjeta.charAt(i));

        if (alt) {
            num *= 2;
            if (num > 9) {
                num = (num % 10) + 1;
            }
        }

        alt = !alt;
        sum += num;
        i--;
    }

    var mul = sum * 9;
    var res = mul % 10;

    if (res % 10 === 0) {

        return alert('El No. de Tarjeta ' + tarjeta + ' su suma es: ' + sum + '  y su numero de verificacion es: ' + res + ' por lo cual queda de la siguiente forma:  ' + tarjeta + res);
    } else {
        return alert('El No. de Tarjeta ' + tarjeta + ' su suma es: ' + sum + '  y su numero de verificacion es: ' + res + ' por lo cual queda de la siguiente forma:  ' + tarjeta + res);
    }
}

Validar();